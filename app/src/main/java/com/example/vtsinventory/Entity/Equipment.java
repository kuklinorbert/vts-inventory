package com.example.vtsinventory.Entity;

import java.io.Serializable;

public class Equipment implements Serializable {
    private int id;
    private String locationId;
    private String category;
    private String type;
    private String location;
    private String faultyId;

    public Equipment(int id, String locationId, String category, String type, String location, String faultyId) {
        this.id = id;
        this.locationId = locationId;
        this.category = category;
        this.type = type;
        this.location = location;
        this.faultyId = faultyId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getFaultyId() {
        return faultyId;
    }

    public void setFaultyId(String faultyId) {
        this.faultyId = faultyId;
    }

}
