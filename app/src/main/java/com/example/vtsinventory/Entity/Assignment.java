package com.example.vtsinventory.Entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Assignment implements Serializable {
    private String id;
    private String date;
    @SerializedName("location_id")
    private String locationID;
    @SerializedName("location_name")
    private String locationName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getID() { return id; }
    public void setID(String value) { this.id = value; }


    public String getDate() { return date; }
    public void setDate(String value) { this.date = value; }

    public String getLocationID() { return locationID; }
    public void setLocationID(String value) { this.locationID = value; }


}
