package com.example.vtsinventory.Entity;

import com.google.gson.annotations.SerializedName;


public class User {
    private String id;
    @SerializedName("first name")
    private String firstName;
    @SerializedName("last name")
    private String lastName;
    private String email;
    private String date;
    private String location;

    public String getID() { return id; }
    public void setID(String value) { this.id = value; }

    public String getFirstName() { return firstName; }
    public void setFirstName(String value) { this.firstName = value; }

    public String getLastName() { return lastName; }
    public void setLastName(String value) { this.lastName = value; }

    public String getEmail() { return email; }
    public void setEmail(String value) { this.email = value; }

    public String getDate() { return date; }
    public void setDate(String value) { this.date = value; }

    public String getLocation() { return location; }
    public void setLocation(String value) { this.location = value; }
}
