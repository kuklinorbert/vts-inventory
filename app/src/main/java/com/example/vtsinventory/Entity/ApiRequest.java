package com.example.vtsinventory.Entity;

import java.util.Map;

public class ApiRequest {
    private String type;
    private String action;

    public ApiRequest(String type, String action, Map<String, Object> input) {
        this.type = type;
        this.action = action;
        this.input = input;
    }

    private Map<String,Object> input;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Map<String, Object> getInput() {
        return input;
    }

    public void setInput(Map<String, Object> input) {
        this.input = input;
    }
}
