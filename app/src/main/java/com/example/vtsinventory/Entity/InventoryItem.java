package com.example.vtsinventory.Entity;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class InventoryItem {
    @SerializedName("equipment_id")
    private String equipmentId;
    @SerializedName("user_id")
    private String userId;
    @SerializedName("location_id")
    private String locationId;
    private Date date;
    @SerializedName("faulty_id")
    private String faultyId;

    public InventoryItem(String equipmentId, String userId, String locationId, Date date, String faultyId) {
        this.equipmentId = equipmentId;
        this.userId = userId;
        this.locationId = locationId;
        this.date = date;
        this.faultyId = faultyId;
    }

    public String getEquipmentId() {
        return equipmentId;
    }

    public void setEquipmentId(String equipmentId) {
        this.equipmentId = equipmentId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getFaultyId() {
        return faultyId;
    }

    public void setFaultyId(String faultyId) {
        this.faultyId = faultyId;
    }
}
