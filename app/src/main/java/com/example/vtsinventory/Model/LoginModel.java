package com.example.vtsinventory.Model;

import com.example.vtsinventory.Contract.LoginContract;
import com.example.vtsinventory.Entity.ApiRequest;
import com.example.vtsinventory.Network.ApiClient;
import com.example.vtsinventory.Network.ApiInterface;
import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginModel implements LoginContract.Model {
    @Override
    public void loginUser(LoginListener listener, String email, String password) {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Map<String, Object> input = new HashMap<>();
        input.put("email",email);
        input.put("password",password);
        Call<JsonObject> call = apiService.login(new ApiRequest("user","login",input));
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                String result = response.body().get("result").getAsJsonObject().get("type").getAsString();
                if(result.equals("success")){
                    List<String> Cookielist = response.headers().values("Set-Cookie");
                    String sessionId = (Cookielist.get(0).split(";"))[0];
                    int userId = response.body().get("result").getAsJsonObject().get("resultid").getAsInt();
                    listener.onSuccess(sessionId,userId);
                }else{
                    System.out.println("fail");
                    listener.onFailure(new Throwable(response.body().get("result").getAsJsonObject().get("message").getAsString()));
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                System.out.println(t.getMessage());
                listener.onFailure(t);
            }
        });

    }
}
