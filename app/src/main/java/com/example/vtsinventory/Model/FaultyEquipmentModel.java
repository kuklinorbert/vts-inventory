package com.example.vtsinventory.Model;

import com.example.vtsinventory.Contract.FaultyEquipmentContract;
import com.example.vtsinventory.Entity.ApiRequest;
import com.example.vtsinventory.Network.ApiClient;
import com.example.vtsinventory.Network.ApiInterface;
import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FaultyEquipmentModel implements FaultyEquipmentContract.Model {
    @Override
    public void sendFaultyEquipment(FaultyEquipmentContract.Model.FaultyEquipmentListener listener, String token, int equipmentId, String comment) {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Map<String, Object > input = new HashMap<>();
        input.put("entry_type","faulty_equipments");
        input.put("id", String.valueOf(equipmentId));
        input.put("comment",comment);
        Call<JsonObject> call = apiService.sendFaultyEquipment(token,new ApiRequest("entry","create",input));
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                String result = response.body().get("result").getAsJsonObject().get("type").getAsString();
                if(result.equals("success")){
                    int faultyEquipmentId = response.body().get("result").getAsJsonObject().get("resultid").getAsInt();
                    listener.onSuccess(faultyEquipmentId);
                }else{
                    listener.onFailure(new Throwable(response.body().get("result").getAsJsonObject().get("message").getAsString()));
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                System.out.println(t.getMessage());
                listener.onFailure(t);
            }
        });
    }
}
