package com.example.vtsinventory.Model;

import com.example.vtsinventory.Contract.ResetPasswordContract;
import com.example.vtsinventory.Entity.ApiRequest;
import com.example.vtsinventory.Network.ApiClient;
import com.example.vtsinventory.Network.ApiInterface;
import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResetPasswordModel implements ResetPasswordContract.Model {

    @Override
    public void resetPassword(ResetPasswordListener listener, int userId, String password, String passwordAgain) {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Map<String, Object> input = new HashMap<>();
        input.put("id", String.valueOf(userId));
        input.put("password", password);
        input.put("rePassword", passwordAgain);
        Call<JsonObject> call = apiService.resetPassword(new ApiRequest("user","forgotpassword",input));
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                System.out.println("hmm");
                System.out.println(response.body());
                String result = response.body().get("result").getAsJsonObject().get("type").getAsString();
                if(result.equals("success")){
                    listener.onSuccess(response.body().get("result").getAsJsonObject().get("message").getAsString());
                }else{
                    listener.onFailure(new Throwable(response.body().get("result").getAsJsonObject().get("message").getAsString()));
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                listener.onFailure(t);
            }
        });
    }
}
