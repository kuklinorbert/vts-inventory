package com.example.vtsinventory.Model;

import com.example.vtsinventory.Contract.UserContract;
import com.example.vtsinventory.Entity.ApiRequest;
import com.example.vtsinventory.Entity.Assignment;
import com.example.vtsinventory.Entity.User;
import com.example.vtsinventory.Network.ApiClient;
import com.example.vtsinventory.Network.ApiInterface;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserModel implements UserContract.Model {

    @Override
    public void getUserData(UserListener listener, String token, int userId) {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Map<String, Object > input = new HashMap<>();
        input.put("id",String.valueOf(userId));
        Call<JsonObject> call = apiService.getUserData(token,new ApiRequest("entry","user",input));
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                String result = response.body().get("result").getAsJsonObject().get("type").getAsString();
                if(result.equals("success")){
                    JsonElement userElement = response.body().get("result").getAsJsonObject().get("user").getAsJsonArray().get(0);
                    User user = new Gson().fromJson(userElement,User.class);
                    JsonElement assignmentElements = response.body().get("result").getAsJsonObject().get("assignments").getAsJsonArray();
                    Type userListType = new TypeToken<List<Assignment>>(){}.getType();
                    List<Assignment> assignments = new Gson().fromJson(assignmentElements, userListType);
                    listener.onSuccess(user,assignments);
                }else{
                    listener.onFailure(new Throwable("Failed to get user data"));
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                listener.onFailure(t);
            }
        });
    }
}
