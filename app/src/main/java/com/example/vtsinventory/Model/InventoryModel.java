package com.example.vtsinventory.Model;

import com.example.vtsinventory.Contract.InventoryContract;
import com.example.vtsinventory.Entity.ApiRequest;
import com.example.vtsinventory.Entity.InventoryItem;
import com.example.vtsinventory.Network.ApiClient;
import com.example.vtsinventory.Network.ApiInterface;
import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InventoryModel implements InventoryContract.Model {
    @Override
    public void sendInventory(InventoryListener listener, String token, List<InventoryItem> equipment) {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Map<String, Object> input = new HashMap<>();
        input.put("inventory_items", equipment);
        Call<JsonObject> call = apiService.sendInventory(token, new ApiRequest("entry","insertinventory",input));
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                String result = response.body().get("result").getAsJsonObject().get("type").getAsString();
                if(result.equals("success")){
                    listener.onSuccess();
                }else{
                    listener.onFailure(new Throwable("Failed to send inventory!"));
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                System.out.println(t.getMessage());
                listener.onFailure(t);
            }
        });
    }
}
