package com.example.vtsinventory.Model;

import com.example.vtsinventory.Contract.EquipmentContract;
import com.example.vtsinventory.Entity.ApiRequest;
import com.example.vtsinventory.Entity.Equipment;
import com.example.vtsinventory.Network.ApiClient;
import com.example.vtsinventory.Network.ApiInterface;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EquipmentModel implements EquipmentContract.Model {
    @Override
    public void getEquipment(EquipmentListener listener, String token, int id) {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Map<String, Object > input = new HashMap<>();
        input.put("id",String.valueOf(id));
        Call<JsonObject> call = apiService.getEquipmentData(token, new ApiRequest("entry","equipment",input));
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                String result = response.body().get("result").getAsJsonObject().get("type").getAsString();
                if(result.equals("success")){
                    JsonElement equipmentElement = response.body().get("result").getAsJsonObject().get("equipment").getAsJsonArray().get(0);
                    Equipment equipment = new Gson().fromJson(equipmentElement, Equipment.class);
                    listener.onSuccess(equipment);
                }else{
                    listener.onFailure(new Throwable(response.body().get("result").getAsJsonObject().get("message").getAsString()));
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                listener.onFailure(t);
            }
        });
    }
}
