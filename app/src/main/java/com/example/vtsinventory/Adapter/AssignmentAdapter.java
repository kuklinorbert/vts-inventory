package com.example.vtsinventory.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.vtsinventory.Entity.Assignment;
import com.example.vtsinventory.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class AssignmentAdapter extends ArrayAdapter<Assignment> {
    private Context adapterContext;
    private List<Assignment> assignmentList;

    public AssignmentAdapter(Context context, List<Assignment> list){
        super(context, 0, list);
        adapterContext = context;
        assignmentList = list;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;

        if(listItem == null){
            listItem = LayoutInflater.from(adapterContext).inflate(R.layout.adapter_assignment,parent,false);
        }

        Assignment currentAssignment = assignmentList.get(position);

        TextView assignmentDate = listItem.findViewById(R.id.adapterInventoryText);
        TextView assignmentLocation = listItem.findViewById(R.id.adapterLocationText);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(currentAssignment.getDate());
            SimpleDateFormat sdfnewformat = new SimpleDateFormat("dd/MM/yyyy");
            String finalDateString = sdfnewformat.format(convertedDate);
            assignmentDate.setText(finalDateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        assignmentLocation.setText(currentAssignment.getLocationName());

        return listItem;
    }
}
