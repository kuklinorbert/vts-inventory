package com.example.vtsinventory.Adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.vtsinventory.Entity.Location;

import java.util.List;

public class SpinnerAdapter extends ArrayAdapter<Location> {
    private Context adapterContext;
    private List<Location> locationList;

    public SpinnerAdapter(Context context, int textViewResourceId, List<Location> list){
        super(context, textViewResourceId, list);
        adapterContext = context;
        locationList = list;
    }

    @Nullable
    @Override
    public Location getItem(int position) {
        return locationList.get(position);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView label = (TextView) super.getView(position,convertView,parent);

        label.setText(locationList.get(position).getName());
        return label;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView label = (TextView) super.getDropDownView(position,convertView,parent);
        label.setText(locationList.get(position).getName());
        return label;
    }
}
