package com.example.vtsinventory.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.vtsinventory.Entity.Equipment;
import com.example.vtsinventory.R;

import java.util.List;

public class InventoryAdapter extends RecyclerView.Adapter<InventoryAdapter.ViewHolder> {

    private LayoutInflater layoutInflater;
    private List<Equipment> equipmentList;

    public InventoryAdapter(Context context, List<Equipment> list){
        layoutInflater = LayoutInflater.from(context);
        equipmentList = list;
    }

    @Override
        public InventoryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.adapter_inventory, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Equipment currentEquipment = equipmentList.get(position);
        holder.equipmentCategory.setText(currentEquipment.getCategory());
        holder.equipmentId.setText(String.valueOf(currentEquipment.getId()));
    }

    @Override
    public int getItemCount() {
        return equipmentList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView equipmentCategory;
        TextView equipmentId;

        ViewHolder(View itemView){
            super(itemView);
            equipmentCategory = itemView.findViewById(R.id.adapterInventoryCategoryText);
            equipmentId = itemView.findViewById(R.id.adapterInventoryIdText);
        }

    }

}
