package com.example.vtsinventory.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.vtsinventory.Contract.ResetPasswordContract;
import com.example.vtsinventory.Presenter.ResetPasswordPresenter;
import com.example.vtsinventory.R;

public class ResetPasswordActivity extends AppCompatActivity implements ResetPasswordContract.View {
    private Bundle bundle;
    private ResetPasswordPresenter resetPasswordPresenter;
    private String password,passwordAgain;
    private EditText passwordText, passwordAgainText;
    private Button savePassword;
    private int userId;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        Toolbar toolbar = findViewById(R.id.my_toolbar_resetpassword);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        resetPasswordPresenter = new ResetPasswordPresenter(this);

        bundle = getIntent().getExtras();
        userId = bundle.getInt("USERID");

        passwordText = findViewById(R.id.passwordTextField);
        passwordAgainText = findViewById(R.id.passwordAgainTextField);
        savePassword = findViewById(R.id.savePasswordButton);
        progressBar = findViewById(R.id.progressBarResetPassword);

        savePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                password = passwordText.getText().toString();
                passwordAgain = passwordAgainText.getText().toString();
                savePassword.setVisibility(View.INVISIBLE);
                progressBar.setVisibility(View.VISIBLE);
                resetPasswordPresenter.resetPassword(userId,password,passwordAgain);
            }
        });

    }

    @Override
    public void resetPasswordSuccess(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
        progressBar.setVisibility(View.INVISIBLE);
        savePassword.setVisibility(View.VISIBLE);
        finish();
    }

    @Override
    public void showMessage(String msg) {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
        progressBar.setVisibility(View.INVISIBLE);
        savePassword.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}