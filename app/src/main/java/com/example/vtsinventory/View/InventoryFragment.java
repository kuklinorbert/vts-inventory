package com.example.vtsinventory.View;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.vtsinventory.Adapter.InventoryAdapter;
import com.example.vtsinventory.Adapter.SpinnerAdapter;
import com.example.vtsinventory.Contract.InventoryContract;
import com.example.vtsinventory.Entity.Equipment;
import com.example.vtsinventory.Entity.InventoryItem;
import com.example.vtsinventory.Entity.Location;
import com.example.vtsinventory.Presenter.InventoryPresenter;
import com.example.vtsinventory.R;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class InventoryFragment extends Fragment implements InventoryContract.View {
    private Button addButton, sendButton;
    private Bundle bundle;
    private ActivityResultLauncher<Intent> launchActivity;
    private String token, locationId;
    private InventoryPresenter inventoryPresenter;
    private RecyclerView equipmentListView;
    private InventoryAdapter inventoryAdapter;
    private List<Location> locationList = new ArrayList<Location>();
    private List<Equipment> equipmentList = new ArrayList<Equipment>();
    private List<InventoryItem> inventoryItemList = new ArrayList<InventoryItem>();
    private ProgressBar progressBar;
    private Spinner spinner;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        launchActivity = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>() {
                    @Override
                    public void onActivityResult(ActivityResult result) {
                        if (result.getResultCode() == Activity.RESULT_OK) {
                            Intent data = result.getData();
                            Bundle bundle = data.getExtras();
                            Equipment equipment = (Equipment) bundle.getSerializable("EQUIPMENT");
                            equipmentList.add(equipment);
                            spinner.setEnabled(false);
                            inventoryAdapter.notifyDataSetChanged();
                        }
                    }
                });

        return inflater.inflate(R.layout.fragment_inventory,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        addButton = view.findViewById(R.id.addEquipmentButton);
        sendButton = view.findViewById(R.id.saveInventoryButton);

        inventoryPresenter = new InventoryPresenter(this);

        bundle = this.getArguments();
        token = bundle.getString("SESSIONID");
        int getUserId = bundle.getInt("USERID");
        locationList = (List<Location>) bundle.getSerializable("locations");

        progressBar = view.findViewById(R.id.progressBarInventory);

        SpinnerAdapter spinnerAdapter = new SpinnerAdapter(getActivity(), R.layout.spinner_item,locationList);
        spinner = view.findViewById(R.id.locationsSpinner);
        spinner.setAdapter(spinnerAdapter);


        equipmentListView = view.findViewById(R.id.equipmentListView);
        inventoryAdapter = new InventoryAdapter(getActivity(),equipmentList);
        equipmentListView.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false));
        equipmentListView.setAdapter(inventoryAdapter);

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), QRScannerActivity.class);
                intent.putExtra("SESSIONID",token);
                intent.putExtra("LOCATIONID", locationId);
                launchActivity.launch(intent);
            }
        });

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(locationId == null) {
                    Toast.makeText(getContext(), "No room selected!", Toast.LENGTH_LONG).show();
                }else if(equipmentList.isEmpty()) {
                    Toast.makeText(getContext(), "Equipment list is empty!", Toast.LENGTH_LONG).show();
                }else{
                    Date date = new Date();
                    for (Equipment item : equipmentList) {
                        inventoryItemList.add(new InventoryItem(String.valueOf(item.getId()), String.valueOf(getUserId), locationId, date, item.getFaultyId() != null ? String.valueOf(item.getFaultyId()) : ""));
                    }
                    inventoryPresenter.sendInventoryData(token, inventoryItemList);
                    sendButton.setVisibility(View.INVISIBLE);
                    progressBar.setVisibility(View.VISIBLE);
                }
            }
        });

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                locationId = String.valueOf(spinnerAdapter.getItem(i).getId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    @Override
    public void sendDataSuccess() {
        equipmentList.clear();
        inventoryAdapter.notifyDataSetChanged();
        sendButton.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.INVISIBLE);
        spinner.setEnabled(true);
        Toast.makeText(getContext(), "Inventory successfully sent!", Toast.LENGTH_LONG).show();
    }

    @Override
    public void showMessage(String msg) {
        Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
        sendButton.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.INVISIBLE);
    }
}
