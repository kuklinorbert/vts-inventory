package com.example.vtsinventory.View;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.vtsinventory.Contract.UserContract;
import com.example.vtsinventory.Entity.Assignment;
import com.example.vtsinventory.Entity.Location;
import com.example.vtsinventory.Entity.User;
import com.example.vtsinventory.Presenter.UserPresenter;
import com.example.vtsinventory.R;
import com.google.android.material.navigation.NavigationView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;

import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends AppCompatActivity implements UserContract.View, NavigationView.OnNavigationItemSelectedListener {
    private DrawerLayout drawer;
    private Toolbar toolbar;
    private Bundle previousActivity, bundle;
    private UserPresenter userPresenter;
    private User user;
    private NavigationView navigationView;
    private int userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        previousActivity = getIntent().getExtras();
        String sessonId = previousActivity.getString("SESSIONID");
        userId = previousActivity.getInt("USERID");


        bundle = new Bundle();
        bundle.putString("SESSIONID", sessonId);
        bundle.putInt("USERID", userId);

        toolbar = findViewById(R.id.main_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_home);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        userPresenter = new UserPresenter(this);
        userPresenter.getUserData(sessonId,userId);

    }

    @Override
    public void onBackPressed() {
        if(drawer.isDrawerOpen(GravityCompat.START)){
            drawer.closeDrawer(GravityCompat.START);
        }else{
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.nav_home:
                Fragment fragmentHome = new HomeFragment();
                fragmentHome.setArguments(bundle);
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragmentHome).commit();
                break;
            case R.id.nav_inventory:
                Fragment fragmentInventory = new InventoryFragment();
                fragmentInventory.setArguments(bundle);
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragmentInventory).commit();
                break;
            case R.id.nav_profile:
                Intent profileIntent = new Intent(this,ProfileActivity.class);
                profileIntent.putExtra("EMAIL",user.getEmail());
                profileIntent.putExtra("USERID",userId);
                profileIntent.putExtra("NAME",user.getFirstName() + "  " + user.getLastName());

                startActivity(profileIntent);
                break;
            case R.id.nav_sign_out:
                Intent signoutIntent = new Intent(this, LoginActivity.class);
                SharedPreferences sp=this.getSharedPreferences("Login", MODE_PRIVATE);
                SharedPreferences.Editor spEditor = sp.edit();
                spEditor.remove("email");
                spEditor.remove("password");
                spEditor.commit();
                startActivity(signoutIntent);
                finish();
                break;
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void getDataSuccess(User userData, List<Assignment> assignments) {
            user = userData;
            Fragment fragment = new HomeFragment();
            List<Location> locations = new ArrayList<>();
            for(Assignment assignment : assignments){
                locations.add(new Location(assignment.getLocationID(),assignment.getLocationName()));
            }
            bundle.putSerializable("locations", (Serializable) locations);
            bundle.putSerializable("assignments", (Serializable) assignments);
            fragment.setArguments(bundle);
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).commit();

    }

    @Override
    public void showMessage(String msg) {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
    }

}