package com.example.vtsinventory.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.vtsinventory.Contract.ForgotPasswordContract;
import com.example.vtsinventory.Presenter.ForgotPasswordPresenter;
import com.example.vtsinventory.R;

public class ForgotPasswordActivity extends AppCompatActivity implements ForgotPasswordContract.View {
    private ForgotPasswordPresenter forgotPasswordPresenter;
    private Button forgotPasswordButton;
    private EditText forgotPasswordText;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        Toolbar toolbar = findViewById(R.id.my_toolbar_forgotpassword);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        forgotPasswordPresenter = new ForgotPasswordPresenter(this);

        forgotPasswordButton = findViewById(R.id.forgotPasswordButton);
        forgotPasswordText = findViewById(R.id.editTextForgot);
        progressBar = findViewById(R.id.progressBarForgotPassword);

        forgotPasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = forgotPasswordText.getText().toString();
                forgotPasswordPresenter.forgotPassword(email);
                forgotPasswordButton.setVisibility(View.INVISIBLE);
                progressBar.setVisibility(View.VISIBLE);
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void forgotPasswordSuccess(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
        forgotPasswordButton.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.INVISIBLE);
        finish();
    }

    @Override
    public void showMessage(String msg) {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
        forgotPasswordButton.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.INVISIBLE);
    }
}