package com.example.vtsinventory.View;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vtsinventory.Contract.LoginContract;
import com.example.vtsinventory.Presenter.LoginPresenter;
import com.example.vtsinventory.R;

public class LoginActivity extends AppCompatActivity implements LoginContract.View {
    private LoginPresenter loginPresenter;
    private Button loginButton;
    private TextView forgotPasswordButton;
    private EditText emailField, passwordField;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor sharedPreferencesEditor;
    private ProgressBar progressBar;
    private String email, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        loginPresenter = new LoginPresenter(this);
        loginButton = findViewById(R.id.loginButton);
        forgotPasswordButton = findViewById(R.id.forgotPasswordTextButton);

        emailField = findViewById(R.id.inputEmail);
        passwordField = findViewById(R.id.inputPassword);
        progressBar = findViewById(R.id.progressBarLogin);

        sharedPreferences = getSharedPreferences("Login", MODE_PRIVATE);
        sharedPreferencesEditor = sharedPreferences.edit();

        checkLogin();

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginPresenter.login(emailField.getText().toString(),passwordField.getText().toString());
                sharedPreferencesEditor.putString("email", emailField.getText().toString());
                sharedPreferencesEditor.putString("password",passwordField.getText().toString());
                sharedPreferencesEditor.commit();
                loginButton.setVisibility(View.INVISIBLE);
                progressBar.setVisibility(View.VISIBLE);
            }
        });

        forgotPasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navigateToForgotPage();
            }
        });
    }

    public void checkLogin(){
        String email = sharedPreferences.getString("email", "");
        String password = sharedPreferences.getString("password", "");
        if(!email.isEmpty() && !password.isEmpty()){
            loginPresenter.login(email,password);
            emailField.setText(email);
            passwordField.setText(password);
            loginButton.setVisibility(View.INVISIBLE);
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    public void navigateToForgotPage(){
        Intent intent = new Intent(this, ForgotPasswordActivity.class);
        startActivity(intent);
    }

    @Override
    public void loginSuccess(String sessionId, int userId) {
        Intent intent = new Intent(this, HomeActivity.class);
        intent.putExtra("SESSIONID",sessionId);
        intent.putExtra("USERID", userId);
        loginButton.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.INVISIBLE);
        startActivity(intent);
        finish();
    }

    @Override
    public void showMessage(String msg) {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
        sharedPreferencesEditor.remove("email");
        sharedPreferencesEditor.remove("password");
        sharedPreferencesEditor.commit();
        loginButton.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.INVISIBLE);
    }
}