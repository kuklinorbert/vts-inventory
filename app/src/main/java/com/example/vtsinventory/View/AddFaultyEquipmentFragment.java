package com.example.vtsinventory.View;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.example.vtsinventory.R;

public class AddFaultyEquipmentFragment extends DialogFragment {

    public interface DialogListener{
        void onDialogActionClick(DialogFragment dialog, String comment);
        void onDialogNegativeClick(DialogFragment dialog);
    }
    public AddFaultyEquipmentFragment() {
    }

    DialogListener listener;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_faultyequipment,null);

        builder.setView(view).setPositiveButton("Add", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                EditText editText = view.findViewById(R.id.faultCommentEditText);
                listener.onDialogActionClick(AddFaultyEquipmentFragment.this, editText.getText().toString());
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                listener.onDialogNegativeClick(AddFaultyEquipmentFragment.this);
                          }
        });


        return builder.create();
    }

    @Override
    public void onStart() {
        super.onStart();
        ((AlertDialog) getDialog()).getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.rgb(87,111,247));
        ((AlertDialog) getDialog()).getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.rgb(87,111,247));
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try{
            listener = (DialogListener) context;
        }catch (ClassCastException e){
            throw new ClassCastException(getActivity().toString() + " implementálnia kell az DialogListener-t!");
        }
    }

    @Override
    public void onCancel(@NonNull DialogInterface dialog) {
        listener.onDialogNegativeClick(AddFaultyEquipmentFragment.this);
        super.onCancel(dialog);
    }

}