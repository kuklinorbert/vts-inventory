package com.example.vtsinventory.View;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;


import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.budiyev.android.codescanner.CodeScanner;
import com.budiyev.android.codescanner.CodeScannerView;
import com.budiyev.android.codescanner.DecodeCallback;
import com.example.vtsinventory.Contract.EquipmentContract;
import com.example.vtsinventory.Entity.Equipment;
import com.example.vtsinventory.Presenter.EquipmentPresenter;
import com.example.vtsinventory.R;
import com.google.zxing.Result;

import java.io.Serializable;


public class QRScannerActivity extends AppCompatActivity implements EquipmentContract.View {
    private CodeScanner mCodeScanner;
    private int CAMERA_REQUEST_CODE = 101;
    private EquipmentPresenter equipmentPresenter;
    private Equipment equipmentResult;
    private Bundle bundle;
    private String token, locationId;
    private ActivityResultLauncher<Intent> launchActivity;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrscanner);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("QR Scanner");

        equipmentPresenter = new EquipmentPresenter(this);
        bundle = getIntent().getExtras();
        token = bundle.getString("SESSIONID");
        locationId = bundle.getString("LOCATIONID");
        progressBar = findViewById(R.id.progressBarQRScanner);

        int permission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if(permission != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.CAMERA},CAMERA_REQUEST_CODE );
        }else{
            setUpScanner();
            launchActivity = registerForActivityResult(
                    new ActivityResultContracts.StartActivityForResult(),
                    new ActivityResultCallback<ActivityResult>() {
                        @Override
                        public void onActivityResult(ActivityResult result) {
                            if (result.getResultCode() == Activity.RESULT_OK) {
                                Intent data = result.getData();
                                setResult(RESULT_OK, data);
                                finish();
                            }else{
                                mCodeScanner.startPreview();
                            }
                        }
                    });
        }
    }

    public void setUpScanner(){
        CodeScannerView scannerView = findViewById(R.id.qrScanner);
        mCodeScanner = new CodeScanner(this, scannerView);
        mCodeScanner.setDecodeCallback(new DecodeCallback() {
            @Override
            public void onDecoded(@NonNull final Result result) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            equipmentPresenter.getEquipmentData(token, Integer.parseInt(result.getText()));
                            progressBar.setVisibility(View.VISIBLE);
                        }catch (Exception e){
                            showMessage("Error reading QR code!");
                        }
                    }
                });
            }
        });
        mCodeScanner.startPreview();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == CAMERA_REQUEST_CODE) {
            if (grantResults.length == 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                finish();
            } else {
                setUpScanner();
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    @Override
    public void getEquipmentSuccess(Equipment equipment) {
        if(equipment.getLocationId().equals(locationId)) {
            Intent intent = new Intent(this, EquipmentActivity.class);
            intent.putExtra("TOKEN", token);
            intent.putExtra("EQUIPMENT", (Serializable) equipment);
            equipmentResult = equipment;
            progressBar.setVisibility(View.INVISIBLE);
            launchActivity.launch(intent);
        }else{
            showMessage("Equipment doesn't belong in the room");
        }
    }

    @Override
    public void showMessage(String msg) {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
        progressBar.setVisibility(View.INVISIBLE);
        mCodeScanner.startPreview();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}