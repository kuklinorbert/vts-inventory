package com.example.vtsinventory.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vtsinventory.Contract.FaultyEquipmentContract;
import com.example.vtsinventory.Entity.Equipment;
import com.example.vtsinventory.Presenter.FaultyEquipmentPresenter;
import com.example.vtsinventory.R;

import java.io.Serializable;

public class EquipmentActivity extends AppCompatActivity implements FaultyEquipmentContract.View, AddFaultyEquipmentFragment.DialogListener{
    private Bundle bundle;
    private TextView equipmentId, equipmentName,equipmentCategory,equipmentLocation;
    private CheckBox faultyEquipmentCheckBox;
    private FaultyEquipmentPresenter faultyEquipmentPresenter;
    private String token;
    private Equipment equipment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_equipment);
        bundle = getIntent().getExtras();
        equipment = (Equipment) bundle.getSerializable("EQUIPMENT");
        token = bundle.getString("TOKEN");

        Toolbar toolbar = findViewById(R.id.equipment_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        toolbar.inflateMenu(R.menu.appbar_menu);
        faultyEquipmentPresenter = new FaultyEquipmentPresenter(this);

        equipmentId = findViewById(R.id.equipmentCodeTextView);
        equipmentName = findViewById(R.id.equipmentNameTextView);
        equipmentCategory = findViewById(R.id.equipmentCategoryTextView);
        equipmentLocation = findViewById(R.id.equipmentLocationTextView);

        equipmentId.setText(String.valueOf(equipment.getId()));
        equipmentName.setText(equipment.getType());
        equipmentCategory.setText(equipment.getCategory());
        equipmentLocation.setText(equipment.getLocation());

       faultyEquipmentCheckBox = findViewById(R.id.faultyEquipmentCheckBox);

        Button testButton = toolbar.findViewById(R.id.buttonAppbar);
        testButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.putExtra("EQUIPMENT", (Serializable) equipment);
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        faultyEquipmentCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment dialog = new AddFaultyEquipmentFragment();
                dialog.show(getSupportFragmentManager(), "AddFaultyEquipmentFragment");
            }
        });
    }



    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        System.out.println(item);
        System.out.println(id);
        if (id == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDialogActionClick(DialogFragment dialog, String comment) {
        faultyEquipmentPresenter.sendFaultDescription(token, equipment.getId(),comment);
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
        dialog.dismiss();
        faultyEquipmentCheckBox.setChecked(false);
    }

    @Override
    public void sendFaultSuccess(int faultyId) {
        faultyEquipmentCheckBox.setChecked(true);
        faultyEquipmentCheckBox.setClickable(false);
        equipment.setFaultyId(String.valueOf(faultyId));
        Toast.makeText(getApplicationContext(), "Fault description saved!", Toast.LENGTH_LONG).show();
    }

    @Override
    public void showMessage(String msg) {
        faultyEquipmentCheckBox.setChecked(false);
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
    }
}