package com.example.vtsinventory.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.vtsinventory.R;

public class ProfileActivity extends AppCompatActivity {
    private Bundle bundle;
    private int userId;
    private String email,name;
    private TextView emailTextView, nameTextView;
    private Button resetPasswordButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Toolbar toolbar = findViewById(R.id.my_toolbar_profile);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        bundle = getIntent().getExtras();
        userId = bundle.getInt("USERID");
        name = bundle.getString("NAME");
        email = bundle.getString("EMAIL");

        nameTextView = findViewById(R.id.userNameTextView);
        nameTextView.setText(name);

        emailTextView = findViewById(R.id.userEmailTextView);
        emailTextView.setText(email);

        resetPasswordButton = findViewById(R.id.changePasswordButton);

        resetPasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetPasswordPage();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void resetPasswordPage(){
        Intent intent = new Intent(this, ResetPasswordActivity.class);
        intent.putExtra("USERID",userId);
        startActivity(intent);
    }
}