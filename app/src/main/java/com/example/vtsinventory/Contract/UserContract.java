package com.example.vtsinventory.Contract;

import com.example.vtsinventory.Entity.Assignment;
import com.example.vtsinventory.Entity.User;

import java.util.List;

public interface UserContract {

    interface Model {
        interface UserListener{
            void onSuccess(User user, List<Assignment> assignment);
            void onFailure(Throwable t);
        }

        void getUserData(final UserContract.Model.UserListener listener, String token, int userId);
    }

    interface View{

        void getDataSuccess(User user, List<Assignment> assignments);
        void showMessage(String msg);
    }

    interface Presenter{
        void getUserData(String token, int userId);
    }
}
