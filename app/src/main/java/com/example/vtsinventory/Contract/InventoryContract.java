package com.example.vtsinventory.Contract;

import com.example.vtsinventory.Entity.InventoryItem;

import java.util.List;

public interface InventoryContract {

    interface Model {
        interface InventoryListener{
            void onSuccess();
            void onFailure(Throwable t);
        }

        void sendInventory(final InventoryListener listener, String token, List<InventoryItem> equipments);
    }

    interface View{

        void sendDataSuccess();
        void showMessage(String msg);
    }

    interface Presenter{
        void sendInventoryData(String token, List<InventoryItem> equipments);
    }

}
