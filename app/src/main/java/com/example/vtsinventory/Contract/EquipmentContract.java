package com.example.vtsinventory.Contract;

import com.example.vtsinventory.Entity.Equipment;

public interface EquipmentContract {
    interface Model {
        interface EquipmentListener{
            void onSuccess(Equipment equipment);
            void onFailure(Throwable t);
        }

        void getEquipment(final EquipmentListener listener, String token, int id);
    }

    interface View{
        void getEquipmentSuccess(Equipment equipment);
        void showMessage(String msg);
    }

    interface Presenter{
        void getEquipmentData(String token, int id);
    }
}
