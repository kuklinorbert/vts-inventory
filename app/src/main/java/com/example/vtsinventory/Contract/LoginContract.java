package com.example.vtsinventory.Contract;

public interface LoginContract {

    interface Model {
        interface LoginListener{
            void onSuccess(String sessionId, int userId);
            void onFailure(Throwable t);
        }

        void loginUser(final LoginListener listener,String email, String password);
    }

    interface View{
        void loginSuccess(String sessionId, int userId);
        void showMessage(String msg);
    }

    interface Presenter{
        void login(String email, String password);
    }
}
