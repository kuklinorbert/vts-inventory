package com.example.vtsinventory.Contract;

public interface FaultyEquipmentContract {
    interface Model {
        interface FaultyEquipmentListener{
            void onSuccess(int faultyId);
            void onFailure(Throwable t);
        }

        void sendFaultyEquipment(final FaultyEquipmentContract.Model.FaultyEquipmentListener listener, String token, int equipmentId, String comment);
    }

    interface View{
        void sendFaultSuccess(int faultyId);
        void showMessage(String msg);
    }

    interface Presenter{
        void sendFaultDescription(String token,int equipmentId, String comment);
    }
}
