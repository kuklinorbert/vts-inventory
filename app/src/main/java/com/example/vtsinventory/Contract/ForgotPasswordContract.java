package com.example.vtsinventory.Contract;

public interface ForgotPasswordContract {

    interface Model {
        interface ForgotPasswordListener{
            void onSuccess(String message);
            void onFailure(Throwable t);
        }


        void forgotPassword(final ForgotPasswordListener listener, String email);

    }

    interface View{
        void forgotPasswordSuccess(String msg);
        void showMessage(String msg);
    }

    interface Presenter{
        void forgotPassword(String email);
    }
}
