package com.example.vtsinventory.Contract;

public interface ResetPasswordContract {

    interface Model {

        interface ResetPasswordListener{
            void onSuccess(String message);
            void onFailure(Throwable t);
        }

        void resetPassword(final ResetPasswordListener listener, int userId, String password, String passwordAgain);
    }

    interface View{
        void resetPasswordSuccess(String msg);
        void showMessage(String msg);
    }

    interface Presenter{
        void resetPassword(int userId, String password, String passwordAgain);
    }
}
