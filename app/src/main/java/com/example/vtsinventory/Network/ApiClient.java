package com.example.vtsinventory.Network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class ApiClient {

    private static Retrofit retrofit = null;

    public static Retrofit getClient() {
        if(retrofit == null){
            Gson gson = new GsonBuilder().setLenient().setDateFormat("yyyy-MM-dd' 'HH:mm:ss").create();
            retrofit = new Retrofit.Builder().baseUrl("https://evriting.proj.vts.su.ac.rs/inventory/").addConverterFactory(ScalarsConverterFactory.create()).addConverterFactory(GsonConverterFactory.create(gson)).build();
        }
        return retrofit;
    }
}
