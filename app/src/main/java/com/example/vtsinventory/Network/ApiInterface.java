package com.example.vtsinventory.Network;

import com.example.vtsinventory.Entity.ApiRequest;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface ApiInterface {

    @POST("api")
    Call<JsonObject> login(@Body ApiRequest request);

    @POST("api")
    Call<JsonObject> forgotPassword(@Body ApiRequest request);

    @POST("api")
    Call<JsonObject> resetPassword(@Body ApiRequest request);

    @POST("api")
    Call<JsonObject> getUserData(@Header("Cookie") String token, @Body ApiRequest request);

    @POST("api")
    Call<JsonObject> getEquipmentData(@Header("Cookie") String token, @Body ApiRequest request);

    @POST("api")
    Call<JsonObject> sendInventory(@Header("Cookie") String token, @Body ApiRequest request);

    @POST("api")
    Call<JsonObject> sendFaultyEquipment(@Header("Cookie") String token, @Body ApiRequest request);

}