package com.example.vtsinventory.Presenter;

import com.example.vtsinventory.Contract.UserContract;
import com.example.vtsinventory.Entity.Assignment;
import com.example.vtsinventory.Entity.User;
import com.example.vtsinventory.Model.UserModel;

import java.util.List;

public class UserPresenter implements UserContract.Presenter, UserContract.Model.UserListener {

    private UserContract.View uView;
    private UserContract.Model uModel;

    public UserPresenter(UserContract.View view){
        uView = view;
        uModel = new UserModel();
    }


    @Override
    public void getUserData(String token, int userId) {
        uModel.getUserData(this, token, userId);
    }

    @Override
    public void onSuccess(User user, List<Assignment> assignment) {
        uView.getDataSuccess(user, assignment);
    }

    @Override
    public void onFailure(Throwable t) {
        uView.showMessage(t.getMessage());
    }
}
