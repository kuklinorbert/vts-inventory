package com.example.vtsinventory.Presenter;

import com.example.vtsinventory.Contract.InventoryContract;
import com.example.vtsinventory.Entity.InventoryItem;
import com.example.vtsinventory.Model.InventoryModel;

import java.util.List;

public class InventoryPresenter implements InventoryContract.Presenter,InventoryContract.Model.InventoryListener {

    private InventoryContract.View iView;
    private InventoryContract.Model iModel;

    public InventoryPresenter(InventoryContract.View view){
        iView = view;
        iModel = new InventoryModel();
    }

    @Override
    public void sendInventoryData(String token, List<InventoryItem> equipments) {
        iModel.sendInventory(this,token, equipments);
    }

    @Override
    public void onSuccess() {
        iView.sendDataSuccess();
    }

    @Override
    public void onFailure(Throwable t) {
        iView.showMessage(t.getMessage());
    }
}
