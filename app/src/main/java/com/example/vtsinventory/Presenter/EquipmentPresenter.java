package com.example.vtsinventory.Presenter;

import com.example.vtsinventory.Contract.EquipmentContract;
import com.example.vtsinventory.Entity.Equipment;
import com.example.vtsinventory.Model.EquipmentModel;

public class EquipmentPresenter implements EquipmentContract.Presenter, EquipmentContract.Model.EquipmentListener {

    private EquipmentContract.View eView;
    private EquipmentContract.Model eModel;

    public EquipmentPresenter(EquipmentContract.View view){
        eView = view;
        eModel = new EquipmentModel();
    }

    @Override
    public void getEquipmentData(String token, int id) {
        eModel.getEquipment(this,token, id);
    }

    @Override
    public void onSuccess(Equipment equipment) {
        eView.getEquipmentSuccess(equipment);
    }

    @Override
    public void onFailure(Throwable t) {
        eView.showMessage(t.getMessage());
    }
}
