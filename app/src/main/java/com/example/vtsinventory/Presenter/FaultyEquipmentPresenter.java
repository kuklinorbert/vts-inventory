package com.example.vtsinventory.Presenter;

import com.example.vtsinventory.Contract.FaultyEquipmentContract;
import com.example.vtsinventory.Model.FaultyEquipmentModel;

public class FaultyEquipmentPresenter implements FaultyEquipmentContract.Presenter, FaultyEquipmentContract.Model.FaultyEquipmentListener {

    private FaultyEquipmentContract.View fView;
    private FaultyEquipmentContract.Model fModel;

    public FaultyEquipmentPresenter(FaultyEquipmentContract.View view){
        fView = view;
        fModel = new FaultyEquipmentModel();
    }

    @Override
    public void sendFaultDescription(String token, int equipmentId, String comment) {
        fModel.sendFaultyEquipment(this,token, equipmentId,comment);
    }

    @Override
    public void onSuccess(int faultyId) {
        fView.sendFaultSuccess(faultyId);
    }

    @Override
    public void onFailure(Throwable t) {
        fView.showMessage(t.getMessage());
    }


}
