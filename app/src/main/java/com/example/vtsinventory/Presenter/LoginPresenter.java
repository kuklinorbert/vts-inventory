package com.example.vtsinventory.Presenter;

import com.example.vtsinventory.Contract.LoginContract;
import com.example.vtsinventory.Model.LoginModel;

public class LoginPresenter implements LoginContract.Presenter, LoginContract.Model.LoginListener {

    private LoginContract.View lView;
    private LoginContract.Model lModel;

    public LoginPresenter(LoginContract.View view){
        lView = view;
        lModel = new LoginModel();
    }

    @Override
    public void login(String email, String password) {
        lModel.loginUser(this, email, password);
    }

    @Override
    public void onSuccess(String sessionId, int userId) {
        lView.loginSuccess(sessionId,userId);
    }

    @Override
    public void onFailure(Throwable t) {
        lView.showMessage(t.getMessage());
    }


}
