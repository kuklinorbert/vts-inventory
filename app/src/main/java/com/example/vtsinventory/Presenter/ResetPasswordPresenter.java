package com.example.vtsinventory.Presenter;

import com.example.vtsinventory.Contract.ResetPasswordContract;
import com.example.vtsinventory.Model.ResetPasswordModel;

public class ResetPasswordPresenter implements ResetPasswordContract.Presenter, ResetPasswordContract.Model.ResetPasswordListener {

    private ResetPasswordContract.View rView;
    private ResetPasswordContract.Model rModel;

    public  ResetPasswordPresenter(ResetPasswordContract.View view){
        rView = view;
        rModel = new ResetPasswordModel();
    }

    @Override
    public void resetPassword(int userId, String password, String passwordAgain) {
        rModel.resetPassword(this,userId,password,passwordAgain);
    }


    @Override
    public void onSuccess(String message) {
        rView.resetPasswordSuccess(message);
    }

    @Override
    public void onFailure(Throwable t) {
        rView.showMessage(t.getMessage());
    }
}
