package com.example.vtsinventory.Presenter;

import com.example.vtsinventory.Contract.ForgotPasswordContract;
import com.example.vtsinventory.Model.ForgotPasswordModel;

public class ForgotPasswordPresenter implements ForgotPasswordContract.Presenter, ForgotPasswordContract.Model.ForgotPasswordListener {

    private ForgotPasswordContract.View fView;
    private ForgotPasswordContract.Model fModel;

    public ForgotPasswordPresenter(ForgotPasswordContract.View view){
        fView = view;
        fModel = new ForgotPasswordModel();
    }

    @Override
    public void forgotPassword(String email) {
        fModel.forgotPassword(this,email);
    }


    @Override
    public void onSuccess(String message) {
        fView.forgotPasswordSuccess(message);
    }



    @Override
    public void onFailure(Throwable t) {
        fView.showMessage(t.getMessage());
    }
}
